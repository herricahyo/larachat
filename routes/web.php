<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('api/users', 'Api\V1\UsersController@index'); // retrieving list user yang dapat di chat
Route::post('api/messages', 'Api\V1\MessagesController@index'); // data pesan yang dikirim dan diterima oleh user yang login berdasarkan user yang dipilih
Route::post('api/messages/send', 'Api\V1\MessagesController@store'); // mengirim data pesan ke user yang dipilih
