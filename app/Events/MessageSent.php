<?php

namespace App\Events;

use App\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class MessageSent implements ShouldBroadcastNow
{
  use Dispatchable, InteractsWithSockets, SerializesModels;

  /**
   * @var Message
   */
  public $message;

  /**
   * Create a new event instance.
   *
   * @param Message instance yang dikirim saat event ini dipanggil pada controller MessagesController
   * @return void
   */
  public function __construct(Message $message)
  {
    $this->message = $message;
  }

  /**
   * Get the channels the event should broadcast on.
   *
   * @return \Illuminate\Broadcasting\Channel|array
   */
  public function broadcastOn()
  {
    // return new PrivateChannel('channel-name');
    return new Channel('newMessage-' . $this->message->sender_id . '-' . $this->message->receiver_id);
  }

  // public function broadcastWith()
  // {
  //   return [
  //     'data' => $this->message
  //   ];
  // }
}
