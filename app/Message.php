<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  protected $fillable = [
    'sender_id',
    'receiver_id',
    'message',
  ];

  protected $with = ['sender', 'receiver'];

  /**
   * get data by sender_id
   */
  public function scopeBySender($q, $sender)
  {
    $q->where('sender_id', $sender);
  }

  /**
   * get data by receiver_id
   */
  public function scopeByReceiver($q, $sender)
  {
    $q->where('receiver_id', $sender);
  }

  /**
   * relasi tabel user pengirim data pesan, pesan dimiliki oleh user pengirim
   */
  public function sender()
  {
    return $this->belongsTo(User::class, 'sender_id')->select(['id', 'name']);
  }

  /**
   * relasi tabel user penerima data pesan, pesan dimiliki oleh user penerima
   */
  public function receiver()
  {
    return $this->belongsTo(User::class, 'receiver_id')->select(['id', 'name']);
  }
}
